<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clasificados extends Base_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function categoria() {
        $this->is_logged();
        
        $categoria = $this->uri->segment(3) ? $this->uri->segment(3) : 'inicio';
        $items = $this->categorias_model->obtener_categoria_titulo($categoria);
        
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'clasificados/categoria/'.$categoria;
        $config['total_rows'] = count($items);
        $config['per_page'] = 5;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;
        $config['next_link'] = 'Siguiente';
        $config['last_link'] = '&Uacute;ltima';
        $config['first_link'] = 'Primera';
        $config['prev_link'] = 'Anterior';
        $this->pagination->initialize($config);        
        
        $page = $this->uri->segment(4);
        $this->layout->set('page', $page);
        $this->layout->set('clasificados_items', $this->clasificados_model->items_pagin($items, $config['per_page'], $page));        
        $this->layout->set('pages', $this->pagination->create_links());        
        $this->layout->view('clasificados/view_index');
    }
    
    
    public function detalleadmin() {
        $this->is_logged();
        $this->is_admin();
        
        if($this->uri->segment(5) == 'aprobar') {
            $this->clasificados_model->aprobar_publicacion($this->uri->segment(4));
            redirect('clasificados/categoria/'.$this->uri->segment(3));
        }
        
        if($this->uri->segment(5) == 'rechazar') {
            $this->clasificados_model->rechazar_publicacion($this->uri->segment(4));
            redirect('clasificados/categoria/'.$this->uri->segment(3));
        }
        
        if($this->input->post('submit_comment')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
            if ($this->form_validation->run()) {
                if($id = $this->clasificados_model->save_comment()) {
                    redirect('clasificados/detalleadmin/'.$this->input->post('id_categoria').'/'.$this->input->post('id_item').'/'.$this->input->post('page'));
                }
            }            
        }        
        
        $id_item = $this->uri->segment(4);
        $this->layout->set('comentarios', $this->clasificados_model->obtener_comentarios($id_item));
        $this->layout->set('categoria', 'Detalle de publicaci&oacute;n');
        $this->layout->set('page', $this->uri->segment(5));
        $this->layout->set('item', $this->clasificados_model->get_publicaciones(NULL, $id_item));
        $this->layout->set('images', $this->clasificados_model->get_images_byitem($id_item));
        $this->layout->view('clasificados/view_detalle_admin');
    }
    
    public function detalle() {
        $this->is_logged();
        
        if($this->input->post('submit_comment')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
            if ($this->form_validation->run()) {
                if($id = $this->clasificados_model->save_comment()) {
                    redirect('clasificados/detalle/'.$this->input->post('id_categoria').'/'.$this->input->post('id_item').'/'.$this->input->post('page'));
                }
            }            
        }
        
        $id_item = $this->uri->segment(4);
        $this->layout->set('comentarios', $this->clasificados_model->obtener_comentarios($id_item));
        $this->layout->set('page', $this->uri->segment(5));
        $this->layout->set('item', $this->clasificados_model->get_publicaciones(NULL, $id_item));
        $this->layout->set('images', $this->clasificados_model->get_images_byitem($id_item));
        $this->layout->view('clasificados/view_detalle');
    }    
    
    public function create() {
        $this->is_logged();
        //$this->db->cache_delete_all();
        $categoria_selected = $this->uri->segment(3) ? $this->uri->segment(3) : $this->input->post('categoria');
        $this->layout->set('form_action', 'create');
        $this->layout->set('categorias_ddl', $this->categorias_model->categorias_ddl());
        $this->layout->set('categoria_selected', $categoria_selected);
        $this->layout->set('item', '');
        $this->layout->set('return_url', 'clasificados/categoria/'.$categoria_selected);
        
        if($this->input->post('submit_form_item')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
            $this->form_validation->set_rules('sucursal', 'Sucursal/Sector', 'required|trim');
            $this->form_validation->set_rules('email', 'Mail', 'required|trim|valid_email');
            $this->form_validation->set_rules('categoria', 'Categoría', 'required');
            $this->form_validation->set_rules('usado', 'Nuevo/Usado', 'required');
            $this->form_validation->set_rules('precio', 'Precio', 'required|decimal');
            $this->form_validation->set_rules('title', 'Titulo', 'required|trim|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('description', 'Descripci&oacute;n', 'required|trim|min_length[5]|max_length[255]');
            if ($this->form_validation->run()) {
                if($id = $this->clasificados_model->save()) {
                    redirect('clasificados/upload/'.$this->input->post('categoria').'/'.$id);
                }
            }
        }
        $this->layout->view('clasificados/view_form_item');
    }
    
    public function edit() {
        $this->is_logged();
        $page = $this->input->post('page') ? $this->input->post('page') : $this->uri->segment(5);
        $id_item = $this->uri->segment(4) ? $this->uri->segment(4) : $this->input->post('id_item');
        
        if($this->clasificados_model->get_publicaciones(NULL, $id_item, $this->session->userdata('username')) === FALSE) {
            $this->is_admin();
        }
        
        $id_categoria = $this->uri->segment(3) ? $this->uri->segment(3) : $this->input->post('categoria');
        $this->layout->set('id_item', $id_item);
        $this->layout->set('page', $page);
        $this->layout->set('form_action', 'edit');
        $this->layout->set('return_url', 'clasificados/categoria/'.$id_categoria.'/'.$page);
        $this->layout->set('return_aprobar_url', 'clasificados/pendientesdetalle/'.$id_categoria.'/'.$id_item);
        $this->layout->set('categorias_ddl', $this->categorias_model->categorias_ddl());
        $this->layout->set('item', $this->clasificados_model->get_publicaciones($id_categoria, $id_item));
        $this->layout->set('categoria_selected', $id_categoria);
        
        if($this->input->post('submit_form_item')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('sucursal', 'Sucursal/Sector', 'required|trim');
            $this->form_validation->set_rules('email', 'Mail', 'required|trim|valid_email');            
            $this->form_validation->set_rules('categoria', 'Categoría', 'required');
            $this->form_validation->set_rules('usado', 'Nuevo/Usado', 'required');
            $this->form_validation->set_rules('precio', 'Precio', 'required|decimal');
            $this->form_validation->set_rules('title', 'Titulo', 'required|trim|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('description', 'Descripci&oacute;n', 'required|trim|min_length[5]|max_length[255]');
            if ($this->form_validation->run()) {
                if($id = $this->clasificados_model->save()) {
                    redirect('clasificados/upload/'.$id_categoria.'/'.$id_item.'/'.$page);
                }
            }
        }
        $this->layout->view('clasificados/view_form_item');        
    }


    public function delete() {
        $this->is_logged();
        $id_item = $this->uri->segment(4) ? $this->uri->segment(4) : $this->input->post('id_item');        
        if($this->clasificados_model->get_publicaciones(NULL, $id_item, $this->session->userdata('username')) === FALSE) {
            $this->is_admin();
        }
        if($this->clasificados_model->delete_publicacion($id_item)) {
            $this->session->set_flashdata('layout_message', '<div class="success">La publicación ha sido eliminada con exito.</div>');
            redirect('clasificados/categoria/'.$this->uri->segment(3));
        } else {
            $this->session->set_flashdata('layout_message', '<div class="error">No pudo eliminar la publicación. Inténtalo más tarde.</div>');
            redirect('clasificados/categoria/'.$this->uri->segment(3));            
        }
    }


    public function upload() {
        $this->is_logged();
        $this->layout->set('msg', '');
        $page = $this->input->post('page') ? $this->input->post('page') : $this->uri->segment(5);
        $this->layout->set('page', $page);
        
        if($_FILES && $_FILES['userfile']['name']) {
            $this->load->library('upload');
            $this->load->library('image_lib');
            
            $file_name = md5(rand(1000000, 10000000));
            $file_ext = explode('.', $_FILES['userfile']['name']);        
            $config['file_name'] = $file_name . '.' . $file_ext[1];            
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '2048'; // 2mb
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $this->layout->set('msg', '<div class="error">'.$this->upload->display_errors().'</div>');
            } else {
              
                $config['source_image'] = './uploads/' . $file_name . '.' . $file_ext[1];
                $config['image_library'] = 'gd2';
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = 50;
                $config['width'] = 80;
                $config['height'] = 72;
                $config['create_thumb'] = TRUE;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                $this->image_lib->resize();            
                
                $config['source_image'] = './uploads/' . $file_name . '.' . $file_ext[1];
                $config['image_library'] = 'gd2';
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = 50;
                $config['width'] = 640;
                $config['height'] = 480;
                $config['create_thumb'] = FALSE;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                $this->image_lib->resize();                

                $this->clasificados_model->upload_image($this->upload->data(), $this->input->post('id_item'), $_FILES['userfile']['tmp_name'], $file_name . '_thumb.' . $file_ext[1]);
                $this->session->set_flashdata('msg', '<div class="success">El imagen ha sido subido con exito.</div>');
                redirect('clasificados/upload/'.$this->input->post('id_categoria').'/'.$this->input->post('id_item').'/'.$page);
            }            
        } elseif($_FILES) {
            $this->layout->set('msg', '<div class="error">No ha seleccionado ning&uacute;n archivo para subir</div>');
        }
                
        $id_item = $this->uri->segment(4)?$this->uri->segment(4):$this->input->post('id_item');
        $id_categoria = $this->uri->segment(3)?$this->uri->segment(3):$this->input->post('id_categoria');
        $this->layout->set('id_item', $id_item);
        $this->layout->set('id_categoria', $id_categoria);
        $this->layout->set('item', $this->clasificados_model->get_publicaciones($id_categoria, $id_item));
        $this->layout->set('images', $this->clasificados_model->get_images_byitem($id_item));
        $this->layout->set('return_url', 'clasificados/categoria/'.$id_categoria.'/'.$page);
        $this->layout->set('return_aprobar_url', 'clasificados/detalleadmin/'.$id_categoria.'/'.$id_item);
        $this->layout->view('clasificados/view_form_upload');
    }
    
    public function delete_image() {
        $this->is_logged();
        $this->clasificados_model->delete_image($this->uri->segment(5));
        redirect('clasificados/upload/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(6));
    }
    
    public function mainimage() {
        $this->is_logged();
        $id_item = $this->uri->segment(4);
        $id_image = $this->uri->segment(5);
        $this->layout->set('page', $this->uri->segment(6));
        $this->layout->set('images', $this->clasificados_model->set_main_image($id_item, $id_image));
        redirect('clasificados/upload/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(6));
    }        
    
    
    public function finalizadas() {
        $this->is_logged();
        
        $items = $this->clasificados_model->get_publicaciones(NULL, NULL, $this->session->userdata('username'), 4);
        $this->layout->set('categoria', 'Publlicaciones pendientes');
        
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'clasificados/pendientes';
        $config['total_rows'] = $items ? $items->num_rows() : 0;
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['num_links'] = 2;
        $config['next_link'] = 'Siguiente';
        $config['last_link'] = '&Uacute;ltima';
        $config['first_link'] = 'Primera';
        $config['prev_link'] = 'Anterior';
        $this->pagination->initialize($config);        
        
        $page = $this->uri->segment(3);
        $this->layout->set('page', $page);
        $this->layout->set('clasificados_items', $this->clasificados_model->items_pagin($items, $config['per_page'], $page));        
        $this->layout->set('pages', $this->pagination->create_links());        
        $this->layout->view('clasificados/view_finalizadas');
    }    
    
    public function finalizadasdetalle() {
        $this->is_logged();
        
        if($this->uri->segment(5) == 'republicar') {
            $this->clasificados_model->republicar_publicacion($this->uri->segment(4));
            redirect('clasificados/categoria/'.$this->uri->segment(3));
        }
        
        $id_item = $this->uri->segment(4);
        $this->layout->set('page', $this->uri->segment(5));
        $this->layout->set('item', $this->clasificados_model->get_publicaciones(NULL, $id_item, NULL, 4));
        $this->layout->set('images', $this->clasificados_model->get_images_byitem($id_item));
        $this->layout->view('clasificados/view_finalizadas_detalle');
    }    
}