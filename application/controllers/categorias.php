<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categorias extends Base_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->is_logged();
        $this->is_admin();
        $categorias_admin = $this->categorias_model->get_all_categorias();
        $this->layout->set('categorias_admin', $categorias_admin);
        $this->layout->set('categorias_count', $this->categorias->num_rows());
        $this->layout->view('categorias/view_index');
    }
    
    public function nueva() {
        $this->is_logged();
        $this->is_admin();                
        if($this->input->post('submit_form_categoria')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       
            $this->form_validation->set_rules('categoria', 'Categoría', 'required|min_length[5]|max_length[40]');
            if ($this->form_validation->run()) {
                $this->categorias_model->save();
                redirect('categorias');
            }
        }
        $this->layout->view('categorias/view_form_categorias');        
    }


    public function orden() {
        $this->is_logged();
        $this->is_admin();        
        
        $sentido = $this->uri->segment(3);
        $categoria = $this->uri->segment(4);
        $orden = $this->uri->segment(5);
        $this->categorias_model->save_orden($sentido, $orden, $categoria);
        redirect('categorias');
    }
    
    public function menu() {
        $id_categoria = $this->uri->segment(3);
        $id_estado = $this->uri->segment(4);
        $this->categorias_model->mostrar_ocultar_categoria($id_categoria, $id_estado);
        redirect('categorias');
    }
}