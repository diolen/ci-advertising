<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios extends Base_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        $this->layout->setLayout('layout_bind_empty');
        $this->layout->set('mostrar_header', TRUE);
        $this->layout->set('no_visible_elements', TRUE);

        $this->load->library('form_validation');

        if ($this->input->post('submit_login')) {

            $this->form_validation->set_error_delimiters('<br><span class="error">', '</span>');
            $this->form_validation->set_rules('username', 'Usuario', 'trim|required|min_length[5]|max_length[12]|xss_clean');
            $this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|required|xss_clean');

            if ($this->form_validation->run() != FALSE) {

                $ad = $this->usuarios_model->get_usuario_ad($this->input->post('username'), $this->input->post('username'), $this->input->post('password'));
                
                if ($ad) {
                    $this->session->set_userdata('logged_in', 'TRUE');
                    $this->session->set_userdata('username', $this->input->post('username'));
                    $this->session->set_userdata('ad_displayname', $ad->ad_displayname);
                    $this->session->set_userdata('ad_telefono_interno', $ad->ad_telefono_interno);
                    $this->session->set_userdata('ad_email', $ad->ad_email);
                    
                    $this->load->model('clasificados_model');
                    $this->clasificados_model->caducidad();
                    
                    redirect(base_url());
                } else {
                    $this->session->set_flashdata('flashdata', '<div class="alert alert-error">Usuario o Contrase&ntilde;a incorrecto.</div>');
                    redirect(base_url());
                }
            }
        }
        $this->layout->view('usuarios/view_login');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    public function terminos()
    {
        if($this->input->post('submit_terminos')) {
            $this->usuarios_model->aceptar_terminos_condiciones();
            redirect(base_url());
        }
        $this->layout->setLayout('layout_bind_empty');
        $this->layout->view('usuarios/view_terminos');
    }

}