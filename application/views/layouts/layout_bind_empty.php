<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" itemscope="" itemtype="http://schema.org/WebPage">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Intranet Bind</title>
        <meta http-equiv="X-UA-Compatible" content="chrome=1" />
        <link rel="shortcut icon" type="image/x-icon" href="//www.google.com/images/icons/product/sites-16.ico" />
        <meta name="title" content="Intranet Bind" />
        <meta itemprop="name" content="Intranet Bind" />
        <meta property="og:title" content="Intranet Bind" />
        <link href="<?php echo base_url() ?>css/standard-css-simple-ltr-ltr.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/overlay.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/allthemes-view.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url() ?>css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/opa-icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/colorbox.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.min.js"></script>  
        
    </head>
    <body xmlns="http://www.google.com/ns/jotspot" id="body" class=" es ">
        
        
        
        <div id="sites-page-toolbar" class="sites-header-divider">
            <div xmlns="http://www.w3.org/1999/xhtml" class="sites-header-divider sites-clear">
                <div class="sites-ccc-nav" style="height: 28px;"></div>
            </div>
        </div>
        
        
        
        <div id="sites-chrome-everything-scrollbar">
            <div id="sites-chrome-everything">
                <div id="sites-chrome-page-wrapper" style="direction: ltr">
                    <div id="sites-chrome-page-wrapper-inside">
                        
                        
                        
                        
                        
                        <div xmlns="http://www.w3.org/1999/xhtml" id="sites-chrome-header-wrapper" style="height:auto;">
                            <table id="sites-chrome-header" class="sites-layout-hbox" cellspacing="0" style="height:auto;">
                                <tr class="sites-header-primary-row" id="sites-chrome-userheader">
                                    <td id="sites-header-title" class="sites-chrome-header-valign-middle"><div class="sites-header-cell-buffer-wrapper"><h2><a href="https://sites.google.com/a/bancoindustrial.com.ar/intranet-bind/" id="sites-chrome-userheader-logo"><img id="logo-img-id" src="<?php echo base_url() ?>img/customLogo.jpg" alt="Intranet Bind" class="sites-logo sites-chrome-header-valign-middle" /></a></h2></div></td><td class="sites-layout-searchbox sites-chrome-header-valign-middle"><div class="sites-header-cell-buffer-wrapper"></div></td>
                                </tr>
                                <tr class="sites-header-secondary-row" id="sites-chrome-horizontal-nav">
                                    <td colspan="2" id="sites-chrome-header-horizontal-nav-container">
                                        <?php $this->load->view('navbar') ?>
                                    </td>
                                </tr>
                            </table> 
                        </div> 
                        
                        
                        
                        
                        <div id="sites-chrome-main-wrapper">
                            <div id="sites-chrome-main-wrapper-inside">
                                <table id="sites-chrome-main" class="sites-layout-hbox" cellspacing="0" cellpadding="{scmCellpadding}" border="0">
                                    <tr>
                                        <td id="sites-chrome-sidebar-left" class="sites-layout-sidebar-left" style="display: none; width: 200px">
                                        </td>
                                        <td id="sites-canvas-wrapper">
                                            <div id="sites-canvas">
                                                <div id="sites-canvas-main" class="sites-canvas-main">
                                                    <div id="sites-canvas-main-content" style="height: 500px;">
                                                        
                                                        <?php echo $content ?>                                                        
                                                        
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </td> 
                                    </tr>
                                </table> 
                            </div> 
                        </div> 
                        
                        
                        
                        <div id="sites-chrome-footer-wrapper"></div>
                        <div id="sites-chrome-onebar-footer"><?php $this->load->view('footer') ?></div>                        
                        
                        
                    </div> 
                </div> 
            </div> 
        </div> 
    </body>
</html>
