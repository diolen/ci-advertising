<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" itemscope="" itemtype="http://schema.org/WebPage">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Intranet Bind</title>
        <meta http-equiv="X-UA-Compatible" content="chrome=1" />
        <link rel="shortcut icon" type="image/x-icon" href="//www.google.com/images/icons/product/sites-16.ico" />
        <meta name="title" content="Intranet Bind" />
        <meta itemprop="name" content="Intranet Bind" />
        <meta property="og:title" content="Intranet Bind" />
        <link href="<?php echo base_url() ?>css/standard-css-simple-ltr-ltr.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/overlay.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/allthemes-view.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url() ?>css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/opa-icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/colorbox.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.min.js"></script>  
        
    </head>
    <body xmlns="http://www.google.com/ns/jotspot" id="body" class=" es ">
        
        
        
        <div id="sites-page-toolbar" class="sites-header-divider">
            <div xmlns="http://www.w3.org/1999/xhtml" class="sites-header-divider sites-clear">
                <div class="sites-ccc-nav" style="height: 28px;"></div>
            </div>
        </div>
        
        
        
        <div id="sites-chrome-everything-scrollbar">
            <div id="sites-chrome-everything">
                <div id="sites-chrome-page-wrapper" style="direction: ltr">
                    <div id="sites-chrome-page-wrapper-inside">
                        
                        
                        
                        
                        
                        <div xmlns="http://www.w3.org/1999/xhtml" id="sites-chrome-header-wrapper" style="height:auto;">
                            <table id="sites-chrome-header" class="sites-layout-hbox" cellspacing="0" style="height:auto;">
                                <tr class="sites-header-primary-row" id="sites-chrome-userheader">
                                    <td id="sites-header-title" class="sites-chrome-header-valign-middle"><div class="sites-header-cell-buffer-wrapper"><h2><a href="https://sites.google.com/a/bancoindustrial.com.ar/intranet-bind/" id="sites-chrome-userheader-logo"><img id="logo-img-id" src="<?php echo base_url() ?>img/customLogo.jpg" alt="Intranet Bind" class="sites-logo sites-chrome-header-valign-middle" /></a></h2></div></td><td class="sites-layout-searchbox sites-chrome-header-valign-middle"><div class="sites-header-cell-buffer-wrapper"></div></td>
                                </tr>
                                <tr class="sites-header-secondary-row" id="sites-chrome-horizontal-nav">
                                    <td colspan="2" id="sites-chrome-header-horizontal-nav-container">
                                        <?php $this->load->view('navbar') ?>
                                    </td>
                                </tr>
                            </table> 
                        </div> 
                        
                        
                        
                        
                        <div id="sites-chrome-main-wrapper">
                            <div id="sites-chrome-main-wrapper-inside" style="margin-left: 10px;">
                                <table id="sites-chrome-main" class="sites-layout-hbox" cellspacing="0" cellpadding="{scmCellpadding}" border="0">
                                    <tr>
                                        <td id="sites-chrome-sidebar-left" class="sites-layout-sidebar-left" style="display: none; width: 200px">
                                        </td>
                                        <td id="sites-canvas-wrapper">
                                            <div id="sites-canvas">
                                                <div id="sites-canvas-main" class="sites-canvas-main">
                                                    <div id="sites-canvas-main-content" style="height: 700px;">
                                                        

                                                        

        <div id="mainPan">
            <div id="leftPan">
                <ul class="one">
                    <li><a href="<?php echo base_url() ?>clasificados/categoria/inicio">Inicio</a></li>
                    <li class="home">Categor&iacute;as</li>
                    <?php foreach($categorias->result() as $cat): ?>
                        <li><a href="<?php echo base_url() ?>clasificados/categoria/<?php echo $cat->id_categoria ?>"><?php echo $cat->name ?> (<?php echo $cat->items ?>)</a></li>
                    <?php endforeach; ?>
                        
                    <li class="home">Mis publicaciones</li>
                    <li><a href="<?php echo base_url() ?>clasificados/create/<?php echo in_array($action, array('categoria','detalle')) ? $this->uri->segment(3) : ''?>">A&ntilde;adir publicación</a></li>                    
                    <li><a href="<?php echo $count_user_publicaciones_publicadas > 0 ? base_url().'clasificados/categoria/mispublicaciones' : 'javascript:void(0)' ?>">Activas (<?php echo $count_user_publicaciones_publicadas ?>)</a></li>                                           
                    <li><a href="<?php echo $count_user_publicaciones_pendientes > 0 ? base_url().'clasificados/categoria/mispendientes' : 'javascript:void(0)' ?>">Pendientes (<?php echo $count_user_publicaciones_pendientes ?>)</a></li>
                    <li><a href="<?php echo $count_user_publicaciones_finalizadas > 0 ? base_url().'clasificados/categoria/misfinalizadas' : 'javascript:void(0)' ?>">Finalizadas (<?php echo $count_user_publicaciones_finalizadas ?>)</a></li>
                    <li><a href="<?php echo $count_user_publicaciones_rechazadas > 0 ? base_url().'clasificados/categoria/misrechazadas' : 'javascript:void(0)' ?>">Rechazadas (<?php echo $count_user_publicaciones_rechazadas ?>)</a></li>
                        
                        
                    <?php if(in_array($this->session->userdata('username'), $this->config->item('admines'))): ?>
                        <li class="home">Administrador</li>    
                        <li><a href="<?php echo base_url() ?>categorias">Categorias</a></li>                        
                        <li><a href="<?php echo $count_publicaciones_publicadas > 0 ? base_url().'clasificados/categoria/publicadas' : 'javascript:void(0)' ?>">Publ. activas (<?php echo $count_publicaciones_publicadas ?>)</a></li>                    
                        <li><a href="<?php echo $count_publicaciones_pendientes > 0 ? base_url().'clasificados/categoria/pendientes' : 'javascript:void(0)' ?>">Publ. pendientes (<?php echo $count_publicaciones_pendientes ?>)</a></li>
                        <li><a href="<?php echo $count_publicaciones_finalizadas > 0 ? base_url().'clasificados/categoria/finalizadas' : 'javascript:void(0)' ?>">Publ. finalizadas (<?php echo $count_publicaciones_finalizadas ?>)</a></li>
                        <li><a href="<?php echo $count_publicaciones_rechazadas > 0 ? base_url().'clasificados/categoria/rechazadas' : 'javascript:void(0)' ?>">Publ. rechazadas (<?php echo $count_publicaciones_rechazadas ?>)</a></li>
                    <?php endif; ?>
                        
                            
                          
                            
                            
                            
                    <li>&nbsp;</li>                    
                    <li><a href="<?php echo base_url() ?>usuarios/logout">Cerrar sesi&oacute;n</a></li>                    
                </ul>
            </div>
            <div id="rightPan">
                <?php echo $this->session->flashdata('layout_message') ?>
                <div id="bodyPan">
                    <?php echo $content ?>
                </div>
            </div>
        </div>                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </td> 
                                    </tr>
                                </table> 
                            </div> 
                        </div> 
                        
                        
                        
                        <div id="sites-chrome-footer-wrapper"></div>
                        <div id="sites-chrome-onebar-footer"><?php $this->load->view('footer') ?></div>                        
                        
                    </div> 
                </div> 
            </div> 
        </div> 
    </body>
</html>
