<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Clasificados</title>
        <link href="<?php echo base_url() ?>css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/opa-icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/colorbox.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.min.js"></script>
    </head>
    <body>
        <div id="mainPan">
            <div id="leftPan">
                <ul class="one">
                    <li class="home">Categor&iacute;as</li>
                    
                    <?php foreach($categorias->result() as $cat): ?>
                        <li><a href="<?php echo base_url() ?>clasificados/categoria/<?php echo $cat->id_categoria ?>"><?php echo $cat->name ?></a></li>
                    <?php endforeach; ?>
                        
                    <li class="home">Mis publicaciones</li>
                    <li><a href="<?php echo base_url() ?>clasificados/create/<?php echo in_array($action, array('categoria','detalle')) ? $this->uri->segment(3) : ''?>">A&ntilde;adir publicación</a></li>                    
                    <li><a href="<?php echo $count_user_publicaciones_publicadas > 0 ? base_url().'clasificados/categoria/mispublicaciones' : 'javascript:void(0)' ?>">Activas (<?php echo $count_user_publicaciones_publicadas ?>)</a></li>                                           
                    <li><a href="<?php echo $count_user_publicaciones_pendientes > 0 ? base_url().'clasificados/categoria/mispendientes' : 'javascript:void(0)' ?>">Pendientes (<?php echo $count_user_publicaciones_pendientes ?>)</a></li>
                    <li><a href="<?php echo $count_user_publicaciones_finalizadas > 0 ? base_url().'clasificados/categoria/misfinalizadas' : 'javascript:void(0)' ?>">Finalizadas (<?php echo $count_user_publicaciones_finalizadas ?>)</a></li>
                    <li><a href="<?php echo $count_user_publicaciones_rechazadas > 0 ? base_url().'clasificados/categoria/misrechazadas' : 'javascript:void(0)' ?>">Rechazadas (<?php echo $count_user_publicaciones_rechazadas ?>)</a></li>
                        
                        
                    <?php if(in_array($this->session->userdata('username'), $this->config->item('admines'))): ?>
                        <li class="home">Administrador</li>    
                        <li><a href="<?php echo base_url() ?>categorias">Categorias</a></li>                        
                        <li><a href="<?php echo $count_publicaciones_publicadas > 0 ? base_url().'clasificados/categoria/publicadas' : 'javascript:void(0)' ?>">Publ. activas (<?php echo $count_publicaciones_publicadas ?>)</a></li>                    
                        <li><a href="<?php echo $count_publicaciones_pendientes > 0 ? base_url().'clasificados/categoria/pendientes' : 'javascript:void(0)' ?>">Publ. pendientes (<?php echo $count_publicaciones_pendientes ?>)</a></li>
                        <li><a href="<?php echo $count_publicaciones_finalizadas > 0 ? base_url().'clasificados/categoria/finalizadas' : 'javascript:void(0)' ?>">Publ. finalizadas (<?php echo $count_publicaciones_finalizadas ?>)</a></li>
                        <li><a href="<?php echo $count_publicaciones_rechazadas > 0 ? base_url().'clasificados/categoria/rechazadas' : 'javascript:void(0)' ?>">Publ. rechazadas (<?php echo $count_publicaciones_rechazadas ?>)</a></li>
                    <?php endif; ?>
                        
                            
                          
                            
                            
                            
                    <li>&nbsp;</li>                    
                    <li><a href="<?php echo base_url() ?>usuarios/logout">Cerrar sesi&oacute;n</a></li>                    
                </ul>
            </div>
            <div id="rightPan">
                <?php echo $this->session->flashdata('layout_message') ?>
                <div id="bodyPan">
                    <?php echo $content ?>
                </div>
            </div>
        </div>
        
        <div id="sites-chrome-onebar-footer"><?php $this->load->view('footer') ?></div>
    </body>
</html>
