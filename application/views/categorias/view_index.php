<h2 style="float: left; margin-right: 20px;">Categorias</h2>
<div style="float: left;cursor: pointer;">
    <a href="<?php echo base_url() ?>categorias/nueva" class="icon icon-color icon-add" title="A&ntilde;adir categor&iacute;a"></a>
    <a href="<?php echo base_url() ?>categorias/nueva" title="A&ntilde;adir categor&iacute;a">A&ntilde;adir categor&iacute;a</a>
</div>
<br />
<p class="more">&nbsp;</p>
<?php if($categorias): ?>
<p>
    <table cellpadding="4" cellspacing="0" class='table_categorias'>
        <tr>
            <th>Orden</th>            
            <th>Nombre</th>
            <th>Estado</th>
            <th>&nbsp;</th>
        </tr>
    <?php foreach($categorias_admin->result() as $key => $cat): ?>
        <tr>
            <td>
                <?php if($cat->estado > 0): ?>
                    <?php if($key == 0): ?>
                        <span class="icon icon-carat-1-n" title=""></span>
                        <a href="<?php echo base_url() ?>categorias/orden/down/<?php echo $cat->id_categoria ?>/<?php echo $cat->orden ?>"><span class="icon icon-color icon-carat-1-s" title="Mover abajo"></span></a>
                    <?php elseif($key == ($categorias_count -1)): ?>
                        <a href="<?php echo base_url() ?>categorias/orden/up/<?php echo $cat->id_categoria ?>/<?php echo $cat->orden ?>"><span class="icon icon-color icon-carat-1-n" title="Mover arriba"></span></a>
                        <span class="icon icon-carat-1-s" title=""></span>
                    <?php else: ?>
                        <a href="<?php echo base_url() ?>categorias/orden/up/<?php echo $cat->id_categoria ?>/<?php echo $cat->orden ?>"><span class="icon icon-color icon-carat-1-n" title="Mover arriba"></span></a>
                        <a href="<?php echo base_url() ?>categorias/orden/down/<?php echo $cat->id_categoria ?>/<?php echo $cat->orden ?>"><span class="icon icon-color icon-carat-1-s" title="Mover abajo"></span></a>
                    <?php endif; ?>                
                <?php endif; ?>
            </td>
            <td><?php echo $cat->name?></td>
            <td><?php echo $cat->estado == 1 ? '<span class="icon icon-color icon-check" title="Activo"></span>' : '<span class="icon icon-color icon-cancel" title="Inactivo"></span>'?></td>
            <td>
                <?php if($cat->estado == 0): ?>
                    <a href="<?php echo base_url() ?>categorias/menu/<?php echo $cat->id_categoria ?>/1" title="Mostrar en el menu de categorias.">Mostrar</a>
                <?php else: ?>    
                    <a href="<?php echo base_url() ?>categorias/menu/<?php echo $cat->id_categoria ?>/0" title="Ocultar">Ocultar</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</p>
<?php endif; ?>