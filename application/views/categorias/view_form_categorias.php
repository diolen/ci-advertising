<h2>Categoria - Formulario.</h2>
<?php echo form_open('categorias/nueva') ?>
<p>
    Categoría<br />
    <?php echo form_input('categoria', set_value('categoria'), 'style="width:300px; maxlength="40" placeholder="M&aacute;xima 40 caracteres"') ?>
    <?php echo form_error('categoria') ?>
</p>
<br />
<p>
    <?php echo form_button('btn_cancel', '<< Salir sin guardar cambios', 'style="cursor: pointer" onclick="window.location.href=\''. base_url() .'categorias\'"') ?>
    <?php echo form_submit('submit_form_categoria', 'Guardar categoria', 'style="cursor: pointer"') ?>
</p>
<?php echo form_close() ?>