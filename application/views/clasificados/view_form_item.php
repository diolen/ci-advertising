<h2>Paso 1 - descripción de publicación.</h2>
<?php $extra = $form_action == 'edit' ? '/'.$categoria_selected.'/'.$id_item.'/'.$page : '' ?>
<?php echo form_open('clasificados/'.$form_action.$extra) ?>

<p>
    Sucursal/Sector<br />
    <?php echo form_input(array('name'=>'sucursal', 'id'=>'sucursal', 
        'value'=>  $item ? $item->item_user_sucursal : set_value('sucursal'), 
        'placeholder'=>'m&aacute;xima 100 caracteres', 'maxlength'=>'100', 'size'=>'55')) ?>
    <?php echo form_error('sucursal') ?>
</p>
<p>
    Mail<br />
<?php if($form_action == 'edit'): ?>
    <?php echo form_input(array('name'=>'email', 'id'=>'email', 
        'value'=>  $item ? $item->item_user_email : set_value('email'), 
        'placeholder'=>'m&aacute;xima 100 caracteres', 'maxlength'=>'100', 'size'=>'55')) ?>
    <?php echo form_error('email') ?>
<?php else: ?>
    <?php echo form_input(array('name'=>'email', 'id'=>'email', 
        'value'=>  $this->input->post('email') ? $this->input->post('email') : $this->session->userdata('ad_email'), 
        'placeholder'=>'m&aacute;xima 100 caracteres', 'maxlength'=>'100', 'size'=>'55')) ?>
    <?php echo form_error('email') ?>    
<?php endif; ?>    
</p>
<p>
    Categoría<br />
    <?php echo form_dropdown('categoria', $categorias_ddl, 
            $categoria_selected ? $categoria_selected : set_value('categoria')) ?>
</p>
<p>
    Nuevo/Usado<br />
    <?php echo form_dropdown('usado', array('1' => 'Nuevo', '2' => 'Usado'), $item ? $item->item_usado : set_value('usado')) ?>
</p>
<p>
    Precio<br />
    <?php echo form_input('precio', $this->input->post('precio')?$this->input->post('precio'):($item ? str_replace('.',',',$item->item_precio) : set_value('precio')), 'placeholder="decimales separados por coma 0,00" style="width:300px;"') ?>
    <?php echo form_error('precio') ?>
</p>
<p>
    Titulo<br />
    <?php echo form_input(array('name'=>'title', 'id'=>'title', 
        'value'=>  $item ? $item->item_title : set_value('title'), 
        'placeholder'=>'m&aacute;xima 100 caracteres', 'maxlength'=>'100', 'size'=>'55')) ?>
    <?php echo form_error('title') ?>
</p>
<p>
    Descripci&oacute;n<br />
    <?php echo form_textarea(array('name'=>'description', 'id'=>'description', 
        'value'=>  $item ? $item->item_descripcion : set_value('description'), 
        'rows'=>'6', 'maxlength'=>'255')) ?>
    <?php echo form_error('description') ?>
    <br />
    M&aacute;xima 255 caracteres.
    <br />
    <strong>Caracteres ingresados: <span id="count_description_character"></span></strong>
</p>
<br />

<p>
<?php if($form_action == 'edit' && $item->id_estado == 2): ?>
    <?php echo form_button('btn_cancel', '<< Salir sin guardar cambios', 
            'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_aprobar_url . '\'"') ?>
    <?php echo form_hidden('id_item', $id_item) ?>
    <?php echo form_hidden('page', $page) ?>
<?php elseif($form_action == 'edit'): ?>    
    <?php echo form_button('btn_cancel', '<< Salir sin guardar cambios', 
            'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_url . '\'"') ?>
    <?php echo form_hidden('id_item', $id_item) ?>
    <?php echo form_hidden('page', $page) ?>    
<?php else: ?>    
    <?php echo form_button('btn_cancel', '<< Salir sin guardar cambios', 
            'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_url . '\'"') ?>
<?php endif; ?>    
    <?php echo form_submit('submit_form_item', 'Guardar y seguir al paso 2 - subir fotos >>', 
            'style="cursor: pointer"') ?>
</p>

<?php echo form_hidden('user', $this->session->userdata('username')) ?>
<?php echo form_hidden('displayname', $this->session->userdata('ad_displayname')) ?>
<?php echo form_close() ?>

<script>
    var descr = $('#description').val();
    $('#count_description_character').html(descr.length);
    $('#description').keyup(function() {
        descr = $('#description').val();
        $('#count_description_character').html(descr.length);
    });
</script>