<?php if($item): ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.colorbox-min.js"></script>
    <script>
        $(document).ready(function(){
            $(".inline").colorbox({inline:true, width:"700px"})
        });
    </script>

    <h2><?php echo $categoria ?></h2>
    <p class="more">&nbsp;</p>
    <div>
    <?php if($images): ?>  
        <?php foreach ($images->result() as $img): ?>
            <a class="inline" href="#inline_image_<?php echo $img->id_image ?>">
                <div class="item" style="background: url(<?php echo $img->item_thumb ?>) bottom no-repeat;" title="Ampliar"></div>    
            </a>
        <?php endforeach; ?>
        
        <div style='display:none'>
            <?php foreach ($images->result() as $img): ?>
                <div id='inline_image_<?php echo $img->id_image ?>' style='padding:10px; background:#fff;'>
                    <image src="<?php echo $img->item_image ?>" />
                </div>
            <?php endforeach; ?>            
        </div>        
    <?php endif; ?>    
    </div>
    
   <div style="clear: left">
        <span class="fecha"><?php echo date("d/m/Y", $item->created_at) ?></span>&nbsp;|&nbsp;
        <span class="fecha"><?php echo $item->item_user_displayname ?></span>&nbsp;|&nbsp;
        <span class="fecha"><?php echo $item->item_user_sucursal ?></span>&nbsp;|&nbsp;
        <span class="email"><?php echo $item->item_user_email ?></span><br />        
        <span class="list_title"><?php echo $item->item_title ?></span><br />
        <span class="fecha"> Art&iacute;culo <?php echo $item->item_usado == 1 ? 'Nuevo' : 'Usado' ?></span><br />
        <span class="money">$<?php echo precio_punto2coma($item->item_precio) ?></span><br /><br />        
        <?php echo $item->item_descripcion ?>
    </div>    
    
    <p class="more">&nbsp;</p>
    
    <p>
        <?php echo anchor('clasificados/finalizadas', '<< Atr&aacute;s') ?>
        
        <?php if($this->session->userdata('username') == $item->usuario): ?>
            &nbsp;|&nbsp;
            <?php echo anchor('clasificados/finalizadasdetalle/'.$item->id_categoria.'/'.$item->id_item.'/republicar', 
                    'Republicar publicaci&oacute;n', 'onclick="return confirm(\'&iquest;Republicar publicaci&oacute;n?\')"') ?>
        <?php endif; ?>        
    </p>
<?php endif; ?>