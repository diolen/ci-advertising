<h2 style="float: left; margin-right: 20px;">Publicaciones finalizadas</h2>
<br />
<p class="more">&nbsp;</p>
<?php if($clasificados_items): ?>
    <?php foreach($clasificados_items as $item): ?>
        <p>
            <div class="list_image">
                <a href="<?php echo base_url() . 'clasificados/finalizadasdetalle/'.$item->id_categoria.'/'.$item->id_item.'/'.$page ?>">
                    <img src="<?php echo $item->item_thumb ? $item->item_thumb : base_url().'img/default.gif' ?>" title="Leer m&aacute;s" />
                </a>
            </div>

            <div class="list_descipction">
                <?php echo anchor('clasificados/finalizadasdetalle/'.$item->id_categoria.'/'.$item->id_item.'/'.$page, $item->item_title, 'class="list_title" title="Leer m&aacute;s"') ?>
                <br />
                <span class="fecha"> Art&iacute;culo <?php echo $item->item_usado == 1 ? 'Nuevo' : 'Usado' ?></span><br />
                <span class="fecha"><?php echo date("d/m/Y", $item->created_at) ?></span>&nbsp;|&nbsp;
                <span class="fecha"><?php echo $item->item_user_displayname ?></span>&nbsp;|&nbsp;
                <span class="fecha"><?php echo $item->item_user_sucursal ?></span>&nbsp;|&nbsp;
                <span class="email"><?php echo $item->item_user_email ?></span><br />                
                <span class="money"><?php echo precio_punto2coma($item->item_precio) ?></span><br />                
                <?php echo $item->item_descripcion ?>            
            </div>
        </p>
        <p class="more" style="clear:both;">
            <?php echo anchor('clasificados/finalizadasdetalle/'.$item->id_categoria.'/'.$item->id_item.'/'.$page, 'leer m&aacute;s') ?>
        </p>
    <?php endforeach; ?>
        <p style="text-align: center;"><?php echo $pages ?></p>
<?php else: ?>
        No hay publicaciones para &eacute;sta categor&iacute;a. 
<?php endif; ?>