<?php if($item): ?>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.colorbox-min.js"></script>
    <script>
        $(document).ready(function(){
            $(".inline").colorbox({inline:true, width:"700px"})
        });
    </script>

    <h2><?php echo $categoria ?></h2>
    <p class="more">&nbsp;</p>
    <div>
    <?php if($images): ?>  
        <?php foreach ($images->result() as $img): ?>
            <a class="inline" href="#inline_image_<?php echo $img->id_image ?>">
                <div class="item" style="background: url(<?php echo $img->item_thumb ?>) bottom no-repeat;" title="Ampliar"></div>    
            </a>
        <?php endforeach; ?>
        
        <div style='display:none'>
            <?php foreach ($images->result() as $img): ?>
                <div id='inline_image_<?php echo $img->id_image ?>' style='padding:10px; background:#fff;'>
                    <image src="<?php echo $img->item_image ?>" />
                </div>
            <?php endforeach; ?>            
        </div>        
        
    <?php else: ?>
        <img src="<?php echo base_url().'img/default.gif' ?>" title="Leer m&aacute;s" />
    <?php endif; ?>    
    </div>
    
   <div style="clear: left">
        <span class="fecha"><?php echo date("d/m/Y", $item->created_at) ?></span>&nbsp;|&nbsp;
        <span class="fecha"><?php echo $item->item_user_displayname ?></span>&nbsp;|&nbsp;
        <span class="fecha"><?php echo $item->item_user_sucursal ?></span>&nbsp;|&nbsp;
        <span class="email"><?php echo $item->item_user_email ?></span><br />        
        <span class="list_title"><?php echo $item->item_title ?></span><br />
        <span class="fecha"> Art&iacute;culo <?php echo $item->item_usado == 1 ? 'Nuevo' : 'Usado' ?></span><br />
        <span class="money">$<?php echo precio_punto2coma($item->item_precio) ?></span><br /><br />
        <?php echo $item->item_descripcion ?>
    </div>    
    
    <p class="more">&nbsp;</p>
    
    <p>
        <?php echo anchor('clasificados/categoria/'.$item->id_categoria.'/'.$page, '<< Atr&aacute;s') ?>
        
        <?php if($administrador || $this->session->userdata('username') == $item->usuario): ?>
            <?php echo in_array($item->id_estado, array(1,2)) ? '&nbsp;|&nbsp;' . anchor('clasificados/edit/'.$item->id_categoria.'/'.$item->id_item.'/'.$page, 'modificar') : '' ?>
            <?php echo in_array($item->id_estado, array(2,3,4)) ? '&nbsp;|&nbsp;' . anchor('clasificados/detalleadmin/'.$item->id_categoria.'/'.$item->id_item.'/aprobar', 'aprobar', 'onclick="return confirm(\'&iquest;Aceptar publicaci&oacute;n?\')"') : '' ?>
            <?php echo in_array($item->id_estado, array(1,2,4)) ? '&nbsp;|&nbsp;' . anchor('clasificados/detalleadmin/'.$item->id_categoria.'/'.$item->id_item.'/rechazar', 'rechazar', 'onclick="return confirm(\'&iquest;Rechazar publicaci&oacute;n?\')"') : '' ?>
            <?php echo anchor('clasificados/delete/'.$item->id_categoria.'/'.$item->id_item.'/'.$page, '&nbsp;|&nbsp; eliminar', 'onclick="return confirm(\'Estás a punto de eliminar una publicación.\')"') ?>        
        <?php endif; ?>        
    </p>

<div style="width: 680px; height: 96px; margin-top: 20px;">
        
        <?php echo form_open('clasificados/detalleadmin/'.$item->id_categoria.'/'.$item->id_item.'/'.$page) ?>
        <div style="float: left; margin-top: 10px; margin-right: 10px;">
            <?php echo form_textarea(array(
                'name' => 'comentario',
                'value' => set_value('comentario'), 
                'placeholder' => 'Comentarios. M&aacute;xima 255 caracteres.',
                'maxlength'=>'255',
                'id' => 'comentario',
                'style' => 'width:540px; height: 66px;'))?>
            <?php echo form_error('comentario') ?>
            <br />M&aacute;xima 255 caracteres.
            <strong>Caracteres ingresados: <span id="count_comment_character"></span></strong>
        </div>
        <div style="float: left; margin-top: 10px;">
            <?php echo form_submit('submit_comment', 'Enviar', 'style="width: 72px; height: 72px; cursor: pointer;"')?>
        </div>
        <?php echo form_hidden('id_item', $item->id_item) ?>
        <?php echo form_hidden('id_categoria', $item->id_categoria) ?>
        <?php echo form_hidden('page', $page) ?>
        <?php echo form_close() ?>
    </div>
    
    <br clear="all" />
    
    <div style="width: 680px; height: 142px; margin-top: 20px; border: 1px solid #cccccc; overflow-x: hidden; overflow-y: auto;">    
        <?php if($comentarios): ?>
            <?php foreach($comentarios->result() as $com): ?>
                <div style="margin: 10px; width: 630px;border-bottom: 1px dashed #cccccc;">
                    <span class="fecha"><?php echo date("d/m/Y H:i", $com->created_at) ?></span>&nbsp;|&nbsp;
                    <span class="fecha"><?php echo $com->user_displayname ?></span>&nbsp;|&nbsp;
                    <span class="email"><?php echo $com->user_email ?></span><br />
                    <?php echo $com->comment_body ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>

<script>
    var comment = $('#comentario').val();
    $('#count_comment_character').html(comment.length);
    $('#comentario').keyup(function() {
        comment = $('#comentario').val();
        $('#count_comment_character').html(comment.length);
    });
</script>    