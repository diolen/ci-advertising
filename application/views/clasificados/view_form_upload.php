<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.colorbox-min.js"></script>
<script>
    $(document).ready(function(){
        $(".inline").colorbox({inline:true, width:"700px"})
    });
</script>

<h2>Paso 2 - subir foto</h2>
<p class="more"></p>
M&aacute;xima 5 fotos por cada publicaci&oacute;n.
<div style="margin: 20px 0;">
    <?php if ($images): ?>
        <?php foreach ($images->result() as $img): ?>
            <div class="item" style="background: url(<?php echo $img->item_thumb ?>) bottom no-repeat;<?php echo $img->item_image_main==1 ? 'border-bottom:4px solid red;' : ''?>">
                <?php if($img->item_image_main != 1): ?>
                <a class="icon icon-color icon-image btnitem" 
                    href="<?php echo base_url() ?>clasificados/mainimage/<?php echo $id_categoria ?>/<?php echo $id_item ?>/<?php echo $img->id_image ?>/<?php echo $page ?>" 
                   title="Mostrar en el listado c&oacute;mo foto principal."></a>
                <?php endif; ?>
                <a class="inline icon icon-color icon-zoomin btnitem" href="#inline_image_<?php echo $img->id_image ?>" title="Ampliar"></a>
                <a class="icon icon-color icon-trash btnitem" title="Eliminar" onclick="return confirm('Est&aacute;s a punto de eliminar la foto.');"
                   href="<?php echo base_url() ?>clasificados/delete_image/<?php echo $id_categoria ?>/<?php echo $id_item ?>/<?php echo $img->id_image ?>/<?php echo $page ?>"></a>
            </div>
        <?php endforeach; ?>
        <div style='display:none'>
            <?php foreach ($images->result() as $img): ?>        
                <div id='inline_image_<?php echo $img->id_image ?>' style='padding:10px; background:#fff;'>
                    <image src="<?php echo $img->item_image ?>" />
                </div>
            <?php endforeach; ?>            
        </div>
    <?php endif; ?>
</div>

<div style="clear:left;"></div>

<?php echo $msg ?>
<?php echo $this->session->flashdata('msg') ?>

<div style="clear:left;"></div>

<?php if(!$images || $images->num_rows() < 5): ?>
    <?php echo form_open_multipart('clasificados/upload/'.$id_categoria.'/'.$id_item.'/'.$page) ?>
    <p>
        <?php echo form_upload('userfile') ?>
    </p>
    <br />
    <p>
        <?php echo form_button('btn_cancel', '<< Atr&aacute;s', 'style="cursor: pointer" onclick="window.location.href=\''. base_url().'clasificados/edit/'.$id_categoria.'/'.$id_item.'/'.$page.'\'"') ?>
        <?php echo form_submit('submit_upload', 'Subir foto', 'title="Seleccione archivo"') ?>
        
        <?php if($administrador && $item->id_estado == 2): ?>
            <?php echo form_button('btn_cancel', 'Finalizar', 'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_aprobar_url . '\'"') ?>
        <?php else: ?>    
            <?php echo form_button('btn_cancel', 'Finalizar', 'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_url . '\'"') ?>    
        <?php endif; ?>         
    </p>
    <?php echo form_hidden('id_item', $id_item) ?>
    <?php echo form_hidden('id_categoria', $id_categoria) ?>
    <?php echo form_hidden('page', $page) ?>
    <?php echo form_close() ?>
    
<?php else: ?>
    <?php if($administrador && $item->id_estado == 2): ?>
        <?php echo form_button('btn_cancel', 'Finalizar', 'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_aprobar_url . '\'"') ?>
    <?php else: ?>    
        <?php echo form_button('btn_cancel', 'Finalizar', 'style="cursor: pointer" onclick="window.location.href=\''. base_url() . $return_url . '\'"') ?>    
    <?php endif; ?>    
<?php endif; ?>