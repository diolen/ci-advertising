<div class="row-fluid sortable">
    <div class="box span8" style="min-height:400px">
        <div class="box-header well" data-original-title>
            <h2>Consulta de usuarios</h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table" id="list_result">
                <thead>
                    <tr>
                        <th colspan="5"><input type="text" id="live_search" style="width: 500px;" placeholder="Nombre, Apellido, Interno, Email" /></th>
                    </tr>
                    <tr>
                        <th>Foto</th>
                        <th>Usuario</th>
                        <th>Interno</th>
                        <th>Email</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>   
                <tbody></tbody>
            </table>  
        </div>
    </div><!--/span-->

</div><!--/row-->

<?php echo form_open_multipart(base_url() . 'search/upload/', array('id' => 'form_upload_image')); ?>
<div class="modal hide fade" id="div_upload_foto">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Subir o Cambiar foto de <span id="usuario_titulo" style="color: red"></span></h3>
    </div>
    <div class="modal-body">
        <input type="file" name="userfile" title="SELECCIONAR IMAGEN" />
        <br/><br/>
        <div id="progress">
            <div id="bar"></div>
            <div id="percent">0%</div >
        </div>
        <br/>
        <div id="msg"></div>        
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" title="SALIR">Salir</a>
        <?php echo form_submit('submit_upload', 'Subir foto', 'id="submit_upload" class="btn btn-small btn-primary"'); ?>
    </div>
</div>
<?php echo form_hidden('ad_usuario', '') ?>
<?php echo form_close() ?>

<script>
    var users = <?php echo $usuarios ?>; 
    var base_url = '<?php echo base_url() ?>';
    var fotos = <?php echo $fotos ?>;
</script>