<div><?php echo $this->session->flashdata('flashdata') ?></div>
<div style="width:300px;margin: 0 auto; margin-top: 100px;">
    <?php echo form_open('usuarios/login', 'class="form-horizontal"') ?>
        <fieldset style="width: 180px;">
            <legend><strong>Compartibind</strong></legend>
            <div style="padding:10px;">
                <span class="icon icon-darkgray icon-user"></span>
                <?php echo form_input('username', set_value('username'), 'id="username" placeholder="usuario" class="input-large" autofocus') ?>
                <?php echo form_error('username') ?>
            </div>
            <div style="padding:10px;">
                <span class="icon icon-darkgray icon-locked"></span>
                <?php echo form_password('password', set_value('password'), 'id="password" placeholder="contrase&ntilde;a" class="input-large"') ?>
                <?php echo form_error('password') ?>
            </div>
            <div style="padding:10px;width:160px;text-align: center;">
                <?php echo form_submit('submit_login', 'Aceptar', 'class="btn btn-primary"') ?>
            </div>
        </fieldset>
    <?php echo form_close() ?>    
</div>