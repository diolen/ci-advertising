<div class="terminos">
    <h2>Terminos y Condiciones</h2>
    <p>
    El presente es un espacio de comunidad destinado al intercambio de bienes y servicios entre los colaboradores del Banco Industrial S.A. El mismo tiene por exclusivo fin brindar el beneficio de poder interactuar a través de una plataforma informática, la cual se realiza sin fines comerciales para Banco Industrial SA    
    </p>
    <p>
    Banco Industrial S.A. no asume responsabilidad alguna por los productos o servicios que se promocionen, vendan, alquilen o intercambien de cualquier forma en la misma, los que son de exclusiva responsabilidad de los usuarios. Banco Industrial S.A. se limita a ofrecer la publicación y facilitar la interacción entre los usuarios, siendo totalmente ajeno a las operaciones que se concreten, por lo que no ofrece ni asume garantía alguna de calidad, cumplimiento ni de contratación alguna.
    </p>
    <p>
    Asimismo, queda prohibido el ofrecimiento bajo cualquier forma o condición de bienes o servicios que contravengan cualquier disposición legal o reglamentaria vigente.
    </p>
    <p>
    Banco Industrial S.A. se encuentra facultado a suprimir cualquier publicación en contravención a lo aquí señalado, como también toda aquella que a su exclusivo criterio sea contraria a los objetivos, procesos, reputación y finalidades comerciales de la entidad, que afecten de cualquier modo el normal desarrollo de las tareas de la empresa o agravien o lesionen de forma actual o potencial a cualquier colaborador del Banco Industrial SA o cualquier a de las empresas vinculadas.
    </p>
    <p>
    El servicio podrá ser descontinuado por Banco Industrial SA sin previo aviso. Los comentarios que los colaboradores emitan en la plataforma son de exclusiva responsabilidad de los emisores, no teniendo Banco Industrial S.A. injerencia ni responsabilidad alguna sobre los mismos, sin perjuicio de la faculta de suprimirlos antes mencionada.
    </p>    
</div>

<div style="width: 100%; text-align: center">
    <?php if($this->session->userdata('terminos') != 'aceptados' && $this->session->userdata('logged_in') != 'FALSE'): ?>
        <?php echo form_open('usuarios/terminos') ?>
            <?php echo form_submit('submit_terminos', 'Acepto', 'style="width:80px;cursor:pointer;"') ?>
            <?php echo anchor('usuarios/logout', 'No acepto') ?>
        <?php echo form_close() ?>    
    <?php elseif($this->session->userdata('terminos') == 'aceptados' && $this->session->userdata('logged_in') == 'TRUE'): ?>
        <?php echo anchor(base_url(), 'Volver al inicio') ?>
    <?php else: ?>
        <?php echo anchor(base_url(), 'Volver al login') ?>
    <?php endif; ?>
</div>