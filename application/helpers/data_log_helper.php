<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('data_log'))
{
    function data_log($table,$data,$operation) {
 
        $ci=& get_instance();
        $ci->load->database();
        
        $data['log_usuario']   = $ci->session->userdata('username');
        $data['log_operacion'] = $operation;

        $ci->db->insert($table .'_log', $data);
        $id_log = $ci->db->insert_id();
        if($id_log == '')
            return FALSE;
        else
            return $id_log;

    }   
}
