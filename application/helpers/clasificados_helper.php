<?php

if (!function_exists('staff_fecha_format')) {

    function staff_fecha_format($date_time) {
        return date("d/m/Y H:i", strtotime(substr($date_time, 0, 19)));
    }

}

if (!function_exists('precio_punto2coma')) {

    function precio_punto2coma($precio) {
        return $precio ? str_replace('.',',',$precio) : '0,00';
    }

}