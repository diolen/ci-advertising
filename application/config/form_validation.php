<?php

$config = array(
    'crear_demanda' => array(
        array(
            'field' => 'area',
            'label' => '&Aacute;rea',
            'rules' => 'required'
        ),
        array(
            'field' => 'titulo',
            'label' => 'Titulo',
            'rules' => 'required|trim'
            //'rules' => 'required|trim|callback_title_exists'
        )
        /*,
        array(
            'field' => 'descripcion',
            'label' => 'Observaci&oacute;n',
            'rules' => 'required|trim'
        )
         * 
         */
    ),
    'copiar_demanda' => array(
        array(
            'field' => 'demandas',
            'label' => 'Demandas',
            'rules' => 'required'
        ),
    ),
    'crear_formulario' => array(
        array(
            'field' => 'formularios',
            'label' => 'Formularios',
            'rules' => 'required'
        ),
    ),
    'editar_demanda' => array(
        array(
            'field' => 'area',
            'label' => '&Aacute;rea',
            'rules' => 'required'
        ),
        array(
            'field' => 'titulo',
            'label' => 'Titulo',
            'rules' => 'required'
        )
    ),
    'crear_titulo' => array(
        array(
            'field' => 'titulo',
            'label' => 'Nombre',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'tipo',
            'label' => 'Tipo',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'areas',
            'label' => 'Area',
            'rules' => 'required|trim'
        )
    ),
    'crear_estado' => array(
        array(
            'field' => 'descripcion',
            'label' => 'Descripci&oacute;n',
            'rules' => 'required'
        )
    ),
    'crear_tipodemanda' => array(
        array(
            'field' => 'descripcion',
            'label' => 'Descripci&oacute;n',
            'rules' => 'required'
        )
    ),
    'crear_wiki' => array(
        array(
            'field' => 'titulo',
            'label' => 'Titulo',
            'rules' => 'required'
        )
    ),
    'crear_area' => array(
        array(
            'field' => 'area_letra',
            'label' => 'C&oacute;digo Area',
            'rules' => 'required'
        ),
        array(
            'field' => 'descripcion',
            'label' => 'Descripci&oacute;n',
            'rules' => 'required'
        )
    ),
    'crear_area_rol' => array(
        array(
            'field' => 'areas',
            'label' => 'Areas',
            'rules' => 'required'
        ),
        array(
            'field' => 'roles',
            'label' => 'Roles&oacute;n',
            'rules' => 'required'
        )
    ),
    'crear_fase' => array(
        array(
            'field' => 'titulos',
            'label' => 'Titulos',
            'rules' => 'required'
        )
    ),
    'crear_tarea' => array(
        array(
            'field' => 'titulos',
            'label' => 'Titulos',
            'rules' => 'required'
        )
    ),
    'crear_permiso' => array(
        array(
            'field' => 'rol_id',
            'label' => 'Roles',
            'rules' => 'required'
        ),
        array(
            'field' => 'accion_id',
            'label' => 'Acciones',
            'rules' => 'required'
        )
    ),
    'crear_template' => array(
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required'
        ),
        array(
            'field' => 'tipo',
            'label' => 'Tipo',
            'rules' => 'required'
        )
    ),
    'actualizar_num' => array(
        array(
            'field' => 'num',
            'label' => 'Numero',
            'rules' => 'required'
        )
    ),
    'email' => array(
        array(
            'field' => 'emailaddress',
            'label' => 'EmailAddress',
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|alpha'
        ),
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'required'
        ),
        array(
            'field' => 'message',
            'label' => 'MessageBody',
            'rules' => 'required'
        )
    )
);