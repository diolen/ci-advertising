<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends CI_Controller {
    
    public $categorias;
    public $publicaciones;
    public $publicaciones_publicadas;
    public $publicaciones_pendientes;
    public $publicaciones_finalizadas;
    public $publicaciones_rechazadas;
    public $user_publicaciones_publicadas;
    public $user_publicaciones_pendientes;
    public $user_publicaciones_finalizadas;
    public $user_publicaciones_rechazadas;
    
    private $_admin;

    public function __construct() {
        parent::__construct();
        
        //$this->db->cache_off();
        
        if ($this->session->userdata('logged_in') != 'TRUE') {
            $this->session->set_userdata('logged_in', 'FALSE');
        }
        
        if($this->session->userdata('logged_in') == 'TRUE') {
            
            $this->layout->set('controller', $this->uri->segment(1));
            $this->layout->set('action', $this->uri->segment(2));            
            
            /* SETEAR USUARIO LOGEADO COMO ADMINISTRADOR O REGULAR */ 
            $this->_admin = in_array($this->session->userdata('username'), $this->config->item('admines')) ? TRUE : FALSE;
            $this->layout->set('administrador', $this->_admin);            
            
            /* OBTENER CATEGORIAS DE PUBLICACIONES */
            $this->categorias = $this->categorias_model->get_categorias();
            $this->layout->set('categorias', $this->categorias);
            if(in_array($this->uri->segment(2), 
                    array(
                        'categoria',
                        'detalle', 
                        'pendientesdetalle', 
                        'finalizadasdetalle'
                        )) || $this->uri->segment(2) == "") {
                /* SETEO EL TITULO DE LA PAGINA ACTUAL */
                $this->layout->set('categoria', $this->categoria_actual());
            }            
            
            /* OBTENER TODAS PUBLICACIONES */
            $this->publicaciones = $this->clasificados_model->get_publicaciones();
            
            /* FILTROS Y CONTADORES DE PUBLICACIONES POR ESTADO DE PUBLICACION */
            $this->publicaciones_publicadas = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 1, TRUE);
            $this->publicaciones_pendientes = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 2, TRUE);
            $this->publicaciones_rechazadas = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 3, TRUE);
            $this->publicaciones_finalizadas = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 4, TRUE);
            $this->layout->set('count_publicaciones_publicadas', $this->publicaciones_publicadas > 0 ? count($this->publicaciones_publicadas) : 0);
            $this->layout->set('count_publicaciones_pendientes', $this->publicaciones_pendientes > 0 ? count($this->publicaciones_pendientes) : 0);
            $this->layout->set('count_publicaciones_rechazadas', $this->publicaciones_rechazadas > 0 ? count($this->publicaciones_rechazadas) : 0);
            $this->layout->set('count_publicaciones_finalizadas', $this->publicaciones_finalizadas > 0 ? count($this->publicaciones_finalizadas) : 0);                        
            
            /* FILTROS Y CONTADORES DE PUBLICACIONES DE USUARIO LOGEADO */
            $this->user_publicaciones_publicadas = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 1);
            $this->user_publicaciones_pendientes = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 2);
            $this->user_publicaciones_rechazadas = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 3);
            $this->user_publicaciones_finalizadas = $this->clasificados_model->filtrar_publicaciones_por_estado($this->publicaciones, 4);
            $this->layout->set('count_user_publicaciones_publicadas', $this->user_publicaciones_publicadas > 0 ? count($this->user_publicaciones_publicadas) : 0);
            $this->layout->set('count_user_publicaciones_pendientes', $this->user_publicaciones_pendientes > 0 ? count($this->user_publicaciones_pendientes) : 0);
            $this->layout->set('count_user_publicaciones_rechazadas', $this->user_publicaciones_rechazadas > 0 ? count($this->user_publicaciones_rechazadas) : 0);
            $this->layout->set('count_user_publicaciones_finalizadas', $this->user_publicaciones_finalizadas > 0 ? count($this->user_publicaciones_finalizadas) : 0);
            
            //$this->layout->setLayout('layout_clasificados');
            $this->layout->setLayout('layout_bind');
            $this->layout->setTitle('Clasificados');

            $this->output->enable_profiler(FALSE);            
        }

    }
    
    public function categoria_actual() {
        $categoria_actual = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        foreach($this->categorias->result() as $cat) {
            $categoria[$cat->id_categoria] = $cat->name; 
        }          
        if(isset($categoria)) {
            return isset($categoria[$categoria_actual]) ? $categoria[$categoria_actual] : next($categoria);
        }
        return FALSE;
    }

    public function is_logged()
    {
        if ($this->session->userdata('logged_in') == 'FALSE' || $this->session->userdata('username') == "") {
            $this->session->sess_destroy();           
            redirect('usuarios/login');
        }
        
        $this->usuarios_model->validar_terminos_condiciones();
        if($this->session->userdata('terminos') != 'aceptados') {
            redirect('usuarios/terminos');
        }
    }
    
    public function is_admin() {
        if(!$this->_admin) {
            $this->session->set_flashdata('layout_message', '<div class="error"><strong>Solo para aministrador.</strong></div>');
            redirect(base_url());
        }
        return TRUE;
    }

}
