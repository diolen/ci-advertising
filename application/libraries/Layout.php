<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Layout {

    private $layoutVars = array();
    private $vars = array();
    private $layout = 'layout_clasificados';
    private $title = 'Clasificados';

    function setLayout($template) {
        $this->layout = $template;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function set($varName, $value) {
        $this->vars[$varName] = $value;
    }

    function setGlobal($varName, $value) {
        $this->layoutVars[$varName] = $value;
    }

    /**
     * Fetch template and return it.
     *
     * @param String $template
     */
    function fetch($template) {
        /* @var CI CI_Base */
        $CI = &get_instance();

        $content = $CI->load->view($template, $this->vars, true);

        $this->layoutVars['content'] = $content;
        $this->layoutVars['title'] = $this->title;

        return $CI->load->view('layouts/'.$this->layout, $this->layoutVars, true);
    }

    /**
     * Renders template to $content.
     *
     * @param String $template
     */
    function view($template) {
        echo $this->fetch($template);
    }

}