<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Form Validation Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Validation
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/form_validation.html
 */
class Base_Form_validation extends CI_Form_validation {

	/**
	 * Decimal number
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function decimal($str)
	{
            if(preg_match('/^[\-+]?[0-9]+$/', $str)) {
                return TRUE;
            }
            if(preg_match('/^[\-+]?[0-9]+,[0-9]+$/', $str)) {
                return TRUE;
            }
            return FALSE;
	}

	// --------------------------------------------------------------------

}