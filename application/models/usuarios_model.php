<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_usuario_ad($user_search, $user_logged, $pass_logged,$tipo = 'responsable') {
        $result = FALSE;
        $auth = $this->ldap->authenticate($user_logged, $pass_logged);
        if ($auth['ok']) {
            
            if(!strstr($user_search,'roles-')) {
                $ad = $this->ldap->user_info(strtolower($user_search));
                //echo '<pre>'; print_r($ad);exit;
                $displayname = str_replace(' - EXT', '', $ad[0]['displayname'][0]);
                $sn = isset($ad[0]['sn'][0]) ? ucwords(strtolower(str_replace(' - EXT', '', $ad[0]['sn'][0]))) : '';
                $result = (object) array(
                            'ad_usuario' => isset($ad[0]['samaccountname'][0]) ? $ad[0]['samaccountname'][0] : 'No definido',
                            'ad_telefono_interno' => isset($ad[0]['telephonenumber'][0]) ? $ad[0]['telephonenumber'][0] : 'No definido',
                            'ad_email' => isset($ad[0]['mail'][0]) ? $ad[0]['mail'][0] : 'No definido',
                            'ad_displayname' => isset($ad[0]['displayname'][0]) ? ucwords(strtolower($ad[0]['displayname'][0])) : 'No definido',
                            'ad_name' => isset($ad[0]['givenname'][0]) ? ucwords(strtolower($ad[0]['givenname'][0])) : '',
                            'ad_lastname' => isset($ad[0]['sn'][0]) ? ucwords(strtolower($ad[0]['sn'][0])) : ''
                );
            }
        }
        return $result;
    }
    
    public function validar_terminos_condiciones() 
    {
        $CI = &get_instance();
        if($CI->session->userdata('terminos') != 'aceptados') {
            $CI->db->cache_off();
            $sql = "SELECT COUNT(*) AS usuario FROM clasificados_terminos WHERE usuario = ?";
            $query = $CI->db->query($sql, array($CI->session->userdata('username')));
            if($query && $query->row()->usuario > 0) {
                $CI->session->set_userdata('terminos', 'aceptados');
                $CI->db->cache_on();
                return TRUE;
            }
            return FALSE;
        }
        return TRUE;
    }
    
    public function aceptar_terminos_condiciones() 
    {
        $CI = &get_instance();
        $CI->db->cache_off();
        $sql = "INSERT INTO clasificados_terminos(usuario) VALUES(?)";
        $CI->db->query($sql, array($CI->session->userdata('username')));
        $CI->session->set_userdata('terminos', 'aceptados');
        $CI->db->cache_on();
    }
    
}
