<?php if (!defined('BASEPATH'))exit('No direct script access allowed');

class Clasificados_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get_publicaciones($categoria=null, $item=null, $usuario=null, $estado=null) {
        $and = "";
        if(!is_null($categoria)) 
            $and .= " AND cit.id_categoria = $categoria ";
        if(!is_null($item))
            $and .= " AND cit.id_item = $item ";
        if(!is_null($usuario))
            $and .= " AND cit.usuario = '".$usuario."' ";
        if(!is_null($estado))
            $and .= " AND cit.id_estado = $estado ";        
        
        $sql = "SELECT 
                cit.*, 
                (SELECT item_thumb FROM clasificados_images WHERE id_item = cit.id_item AND item_image_main=1) AS item_thumb, 
                (SELECT COUNT(*) FROM clasificados_comentarios WHERE id_item = cit.id_item) AS comentarios_count 
            FROM clasificados_items As cit WHERE 1=1 $and 
            ORDER BY cit.id_item DESC";
        
        $items = $this->db->query($sql);
        if($items) {
            if($items->num_rows() > 0) {
                if(!is_null($item)) {
                    return $items->row();
                } else {
                    return $items;
                }
            }            
        }
        return FALSE;
    }
    
    public function items_pagin($items, $per_page, $rows=null) {
        $offset = $rows; 
        $items_array = array();
        if($items) {
            foreach($items as $item) {
                $items_array[] = $item;
            }
            return array_slice($items_array, $offset, $per_page);            
        }
        return FALSE;
    }

    public function get_images_byitem($id_item) {
        $sql = "SELECT 
            cim.*,
            cit.item_title
            FROM clasificados_images AS cim 
            LEFT JOIN clasificados_items AS cit ON cit.id_item = cim.id_item 
            WHERE cim.id_item = ? ORDER BY cim.item_image_main DESC";
        $query = $this->db->query($sql, array($id_item));
        if($query->num_rows() > 0)
            return $query;
        return FALSE;
    }

    public function save() 
    {
        $CI = & get_instance();
        $data = $CI->input->post();
        
        if(isset($data['id_item'])) {
            $update_data = array(
                $data['sucursal'], 
                $data['email'], 
                $data['displayname'],                
                $data['categoria'], 
                $data['title'], 
                $data['description'], 
                time(), 
                $data['usado'],
                str_replace(',','.',$data['precio']),
                $data['id_item']
            );
            $sql = "UPDATE clasificados_items SET 
                    item_user_sucursal=?, 
                    item_user_email=?, 
                    item_user_displayname=?, 
                    id_categoria=?, 
                    item_title=?, 
                    item_descripcion=?, 
                    updated_at=?,
                    item_usado=?,
                    item_precio=?
                WHERE id_item=?";
            $CI->db->query($sql, $update_data);
            $this->pendiente_publicacion($data['id_item']);
            return $data['id_item'];
        } else {
            $insert_data = array(
                'item_user_sucursal'=>$data['sucursal'], 
                'item_user_email'=>$data['email'], 
                'item_user_displayname'=>$data['displayname'] ? $data['displayname'] : 'No espesificado', 
                'id_categoria'=>$data['categoria'], 
                'item_title'=>$data['title'], 
                'item_descripcion'=>$data['description'], 
                'usuario'=>$data['user'], 
                'created_at'=>time(),
                'item_usado'=>$data['usado'],
                'item_precio'=>str_replace(',','.',$data['precio']),
            );
            $CI->db->insert('clasificados_items', $insert_data);
            $query = $CI->db->query('SELECT MAX(id_item) AS max_id_item FROM clasificados_items');
            return $query->row()->max_id_item;            
        }
        return FALSE;
    }


    public function upload_image($upload_data, $id_item, $tmpfile, $thumbnail) {
        $path = $upload_data['full_path'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $image_base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        
        @unlink($tmpfile);
        @unlink($upload_data['full_path']);        
        
        $thumb_path = $upload_data['file_path'].$thumbnail;
        $thumb_type = pathinfo($thumb_path, PATHINFO_EXTENSION);
        $thumb_data = file_get_contents($thumb_path);
        $thumb_base64 = 'data:image/' . $thumb_type . ';base64,' . base64_encode($thumb_data);        
        @unlink($thumb_path);
        
        $item_image_main = 0;
        if($this->is_item_image_main($id_item) == 0) {
            $item_image_main = 1;
        }
        
        $sql = "INSERT INTO clasificados_images(id_item, item_thumb, item_image, item_image_main, created_at) VALUES(?,?,?,?,?)";
        $query_insert = $this->db->query($sql, array($id_item, $thumb_base64, $image_base64, $item_image_main, time()));
    }  

    public function is_item_image_main($id_item) {
        $sql = "SELECT id_image FROM clasificados_images WHERE id_item = ? AND item_image_main = 1";
        $query = $this->db->query($sql, array($id_item));
        return $query->num_rows();
    }


    public function set_main_image($id_item, $id_image) {
        $sql = "UPDATE clasificados_images SET item_image_main = 0 WHERE id_item = ?;
            UPDATE clasificados_images SET item_image_main = 1 WHERE id_image = ?;";
        $query_insert = $this->db->query($sql, array($id_item, $id_image)); 
    }


    public function delete_image($id_image) {
        $sql = "DELETE FROM clasificados_images WHERE id_image = ?";
        $this->db->query($sql, array($id_image));
    }
    
    public function aprobar_publicacion($id_item) {
        $sql = "UPDATE clasificados_items SET id_estado = 1, updated_at = ".time()." WHERE id_item = ".$id_item;
        $this->db->query($sql);
    }
    
    public function pendiente_publicacion($id_item) {
        $sql = "UPDATE clasificados_items SET id_estado = 2, updated_at = ".time()." WHERE id_item = ".$id_item;
        $this->db->query($sql);
    }    
    
    public function rechazar_publicacion($id_item) {
        $sql = "UPDATE clasificados_items SET id_estado = 3, updated_at = ".time()." WHERE id_item = ".$id_item;
        $this->db->query($sql);
    }
    
    public function republicar_publicacion($id_item) {
        $sql = "UPDATE clasificados_items SET id_estado = 2, updated_at = ".time()." WHERE id_item = ".$id_item;
        $this->db->query($sql);
    }
    
    public function caducidad() {
        $CI = & get_instance();
        $days = $CI->config->item('item_caducidad');
        $publicacion_caducidad = time() - (86400 * $days);
        $sql = "UPDATE clasificados_items SET id_estado = 4 WHERE updated_at < " . $publicacion_caducidad;
        $this->db->query($sql);
    }
    
    public function filtrar_publicaciones_por_estado($publicaciones, $id_estado, $all=FALSE) {
        if($publicaciones) {
            
            $CI = & get_instance();
            $result = array();
            
            foreach($publicaciones->result() as $pub) {
                if($all) {
                    if($pub->id_estado == $id_estado ) {
                        $result[$pub->id_item] = $pub;
                    }                    
                } else {
                    if($pub->id_estado == $id_estado && $pub->usuario == $CI->session->userdata('username') ) {
                        $result[$pub->id_item] = $pub;
                    }                    
                }
            }            
            return $result;
        }
        return FALSE;
    }
    
    public function filtrar_publicaciones_por_categoria($publicaciones, $id_categoria) {
        if($publicaciones) {
            $result = array();
            foreach($publicaciones as $pub) {
                if($pub->id_categoria == $id_categoria ) {
                    $result[$pub->id_item] = $pub;
                }
            }            
            return $result;
        }
        return FALSE;        
    }
    
    public function obtener_comentarios($id_item=null) {
        $and = '';
        if(!is_null($id_item)) {
            $and .= " AND id_item = $id_item ";
        }
        $sql = "SELECT * FROM clasificados_comentarios WHERE 1=1 $and ORDER BY id_comment DESC";
        $query = $this->db->query($sql);
        
        if($query && $query->num_rows() > 0) {
            return $query;
        }
        return FALSE;
    }
    
    
    public function save_comment() {
        $CI = & get_instance();
        $data = $CI->input->post();
        $insert_data = array(
            'id_item' => $data['id_item'], 
            'comment_body' => $data['comentario'], 
            'user_displayname' => $CI->session->userdata('ad_displayname') ? $CI->session->userdata('ad_displayname') : 'No espesificado', 
            'user_email' => $CI->session->userdata('ad_email') ? $CI->session->userdata('ad_email') : 'No espesificado', 
            'created_at' => time()
            );
        $this->db->insert('clasificados_comentarios', $insert_data);
        return $this->db->insert_id();            
    } 
    
    public function delete_publicacion($id_item)
    {
        $this->db->trans_begin();
        $this->db->delete('clasificados_items', array('id_item' => $id_item));
        $this->db->delete('clasificados_images', array('id_item' => $id_item));
        $this->db->delete('clasificados_comentarios', array('id_item' => $id_item));
        if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        return $this->db->trans_status();
    }

}
