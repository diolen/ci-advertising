<?php if (!defined('BASEPATH'))exit('No direct script access allowed');

class Categorias_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get_all_categorias() {
        $sql = "SELECT * FROM clasificados_categorias ORDER BY orden ASC";
        return $this->db->query($sql);
    }    
    
    public function save_orden($sentido, $orden, $categoria) {
        $sql = "";
        $categoias_count = $this->categorias->num_rows();
        foreach($this->categorias->result() as $key => $val) {
            $cat_array[$key]['id_categoria'] = $val->id_categoria;
            $cat_array[$key]['orden'] = $val->orden;
        }
        
        for($i=0; $i<$categoias_count; $i++) {
            if($cat_array[$i]['id_categoria'] == $categoria) {
                if($sentido == 'up') {
                    $orden_down = $cat_array[$i]['orden'];
                    $categoria_down = $cat_array[$i-1]['id_categoria'];
                    $orden_up = $cat_array[$i-1]['orden'];
                    $sql .= "UPDATE clasificados_categorias SET orden = $orden_up 
                            WHERE id_categoria = $categoria AND estado = 1;
                            UPDATE clasificados_categorias SET orden = $orden_down 
                            WHERE id_categoria = $categoria_down AND estado = 1";
                }
                if($sentido == 'down') {
                    $orden_down = $cat_array[$i+1]['orden'];
                    $categoria_up = $cat_array[$i+1]['id_categoria'];
                    $orden_up = $cat_array[$i]['orden'];
                    $sql .= "UPDATE clasificados_categorias SET orden = $orden_down 
                            WHERE id_categoria = $categoria AND estado = 1;
                            UPDATE clasificados_categorias SET orden = $orden_up 
                            WHERE id_categoria = $categoria_up AND estado = 1";
                } 
            }
        }        
        $this->db->query($sql);
    }


    public function get_categorias() {
        $sql = "SELECT cc.*,
        (SELECT COUNT(*) FROM clasificados_items WHERE id_categoria = cc.id_categoria AND id_estado = 1)  AS items         
        FROM clasificados_categorias AS cc WHERE estado = 1 ORDER BY orden ASC";
        return $this->db->query($sql);
    }
    
    public function create_categoria() {}
    public function modificar_categoria() {}
    public function delete_categoria() {}
    
    public function categorias_ddl() {
        $ddl = array();
        $categorias = $this->get_categorias();
        foreach($categorias->result() as $categoria) {
            $ddl[$categoria->id_categoria] = $categoria->name;
        }
        return $ddl;
    }
    
    public function mostrar_ocultar_categoria($categoria, $estado) {
        $sql = "UPDATE clasificados_categorias SET estado = ? WHERE id_categoria = ?";
        $this->db->query($sql, array($estado, $categoria));
    }
    
    public function save() {
        $CI = & get_instance();
        
        $sql_orden = "SELECT MAX(orden) AS orden FROM clasificados_categorias";
        $query_orden = $this->db->query($sql_orden);
        if($query_orden && $query_orden->num_rows() > 0) {
            $orden = $query_orden->row()->orden + 1;
        } else {
            $orden = 1;
        }
        
        $sql = "INSERT INTO clasificados_categorias(name, orden) VALUES(?,?)";
        $this->db->query($sql, array($CI->input->post('categoria'), $orden));
    }
    
    
    public function obtener_categoria_titulo($categoria)
    {
        $CI = & get_instance();
        
        if($categoria == 'mispublicaciones') {
            $items = $CI->user_publicaciones_publicadas;
            $CI->layout->set('categoria', 'Mis publicaciones');
        } elseif($categoria == 'mispendientes') {
            $items = $CI->user_publicaciones_pendientes;
            $CI->layout->set('categoria', 'Mis publicaciones pendientes');
        } elseif($categoria == 'misfinalizadas') {
            $items = $CI->user_publicaciones_finalizadas;
            $CI->layout->set('categoria', 'Mis publicaciones finalizadas');
        } elseif($categoria == 'misrechazadas') {
            $items = $CI->user_publicaciones_rechazadas;
            $CI->layout->set('categoria', 'Mis publicaciones rechazadas');
        } elseif($categoria == 'publicadas') {
            $CI->is_admin();
            $items = $CI->publicaciones_publicadas;
            $CI->layout->set('categoria', 'Publicaciones publicadas');
        } elseif($categoria == 'pendientes') {
            $CI->is_admin();
            $items = $CI->publicaciones_pendientes;
            $CI->layout->set('categoria', 'Publicaciones pendientes');
        } elseif($categoria == 'finalizadas') {
            $CI->is_admin();
            $items = $CI->publicaciones_finalizadas;
            $CI->layout->set('categoria', 'Publicaciones finalizadas');
        } elseif($categoria == 'rechazadas') {
            $CI->is_admin();
            $items = $CI->publicaciones_rechazadas;
            $CI->layout->set('categoria', 'Publicaciones rechazadas');
        } elseif($categoria == 'inicio') {
            $items = $CI->publicaciones_publicadas;
            $CI->layout->set('categoria', 'Inicio');
        } else {
            $CI->load->model('clasificados_model');
            $items = $CI->clasificados_model->filtrar_publicaciones_por_categoria($CI->publicaciones_publicadas, $categoria);
        }  
        
        return $items;
    }
}
    